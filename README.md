# sisop-praktikum-modul-4-2023-BJ-A06

## Laporan Pengerjaan Soal Shift Modul 4 Praktikum Sistem Operasi

### Daftar Anggota :
1. Lihardo Marson Purba - 5025211238
2. Farhan Dwi Putra     - 5025211093
3. Victor Gustinova     - 5025211159

### Soal 1
Pada soal ini secara garis besar kita diminta untuk melakukan upload image yang kita telah buat ke docker hub dan mencoba menjalankan image-nya dari folder barcelona dan Napoli yang kita ciptakan dalam PC kita.

- Pada sub soal A dan B kita diminta untuk membuat program yang filenya dinamakan storage.c yang melakukan perintah :
  - melakukan download dataset dari kaggle 
  - unzip file yang didownload
  - membaca file CSV khusus bernama FIFA23_official_data.csv dan mencetak data pemain yang berusia di bawah 25 tahun, memiliki potensi di atas 85, dan bermain di klub lain selain Manchester City. Informasi yang dicetak mencakup nama pemain, klub tempat mereka bermain, umur, potensi, URL foto mereka, dan data lainnya. 

- [ ]  storage.c

```sh
#include <stdio.h>
#include <stdlib.h>

int main (){
  system ("kaggle datasets download -d bryanb/fifa-player-stats-database "); //--path /home/lihardo04/Desktop/soal_1
  system ("unzip  fifa-player-stats-database.zip"); // /home/lihardo04/Desktop/soal_1/
  system ("rm fifa-player-stats-database.zip"); // /home/lihardo04/Desktop/soal_1/
  system("awk -F',' 'NR>1 && $3<25 && $8>85 && $9!=\"Manchester City\" {print}' FIFA23_official_data.csv"); // /home/lihardo04/Desktop/soal_1
  return 0;
}

```
- Sub soal C kita diminta untuk membuat docker image untuk menjalankan storage.c, oleh karena itu kita melakukan konfigurasi image dengan menggunakan docker file untuk melengkapi image dengan file - file yang dibutuhkan serta kebutuhan sistem operasi yang perlu didownload agar program berjalan dengan baik.

- [ ]  Dockerfile

```sh
FROM python:3.10.7

# Install additional dependencies
RUN apt-get update && apt-get install -y unzip

# Install Kaggle
RUN pip install kaggle

# Set the working directory
WORKDIR /app

# Copy the source code file into the container
COPY storage.c .
COPY kaggle.json /root/.kaggle/kaggle.json

# Compile program
RUN gcc storage.c -o get

# Download the dataset and run the program
CMD ["./get"]

}

```
<br>

> image yang telah dibuat

![WhatsApp_Image_2023-06-03_at_19.15.30](/uploads/d66cd4c4a3fffad001aef47d0d5f2826/WhatsApp_Image_2023-06-03_at_19.15.30.jpeg)

<br>

<br>

> Docker run

![WhatsApp_Image_2023-06-03_at_19.15.30__1_](/uploads/e4d28e33a80a767f3175a5cb03b33335/WhatsApp_Image_2023-06-03_at_19.15.30__1_.jpeg)

<br>

- Pada sub soal D kita diminta untuk publish image yang telah dibuat ke dalam docker hub.

<br>

> Hasil

![WhatsApp_Image_2023-06-03_at_19.27.24](/uploads/275d4fc7dac9d2339d94149f061cd886/WhatsApp_Image_2023-06-03_at_19.27.24.jpeg)

<br>

- Sub soal E kita diminta untuk melakukan run terhadap image yang telah di upload dari directory Barcelona dan Napoli yang dijalankan dengan dari docker compose yang dibuat ke dalam setiap directory tadi.

- [ ] Docker compose

```sh
version: '3'
services:
  app:
    image: lihardo-app
    deploy:
      replicas: 5
    ports:
      - 80
    command: chmod 600 /root/.kaggle/kaggle.json && python app.py

```
<br>

> Hasil run - tanpa output awk ke command line

![WhatsApp_Image_2023-06-03_at_19.39.10](/uploads/e2ddd742d0f1d16d7472c2dea2ab7151/WhatsApp_Image_2023-06-03_at_19.39.10.jpeg)

<br>

<br>

> Hasil run - dengan output awk ke command line

![WhatsApp_Image_2023-06-03_at_19.37.02](/uploads/6bf3f6a54ac6207a59b8cb52c4c167e9/WhatsApp_Image_2023-06-03_at_19.37.02.jpeg)

- [ ] NOTE : Hasil output masih panjang ke bawah

<br>

### Soal 2
Pada soal ini diminta untuk membuat FUSE yang mengatur aksi yang dapat dilakukan user berdasarkan path filenya serta mencatat lognya.

- Operasi yang dapat dilakukan adalah operasi read, make, delete, rename. Dibuat struct fuse_operations sebagai berikut.
```sh
static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .read = xmp_read,
    .mkdir = xmp_mkdir,
    .rmdir = xmp_rmdir,
    .unlink = xmp_unlink,
    .rename = xmp_rename,
};

```
- Setiap operasi dilakukan maka dibuat log mengenai operasi tersebut. Dibuat fungsi log sebagai berikut. Akan diambil status (SUCCESS or FAILED), command(MKDIR etc), dan action (make directory ...). Digunakan localtime untuk mengambil waktu dan getlogin untuk mengambil nama user. Log akan dicatat pada file txt yang di open dengan fopen().
```sh
void createLog(const char *status, const char *command, const char *action){
    time_t current_time = time(NULL);
    struct tm *local_time = localtime(&current_time);

    char *buf;
    buf = getlogin();

    // Open the log file in append mode
    FILE *log_file = fopen("/home/victorgg345/Documents/SistemOperasi/Modul4/Nomor2/logmucatatsini.txt", "a");
    if (log_file == NULL) {
        fprintf(stderr, "Error opening log file\n");
        return;
    }

    char usraction[maxSize];
    sprintf(usraction, "%s-%s", buf, action);
    // Format the timestamp
    char timestamp[20];
    strftime(timestamp, sizeof(timestamp), "%d/%m/%Y-%H:%M:%S", local_time);

    // Write the log entry to the file
    fprintf(log_file, "%s::%s::%s::%s\n", status, timestamp, command, usraction);

    // Close the log file
    fclose(log_file);
    
}
```

- Untuk operasi rename dan delete diperlukan pengecekan path. Jika terdapat kata "restricted" maka operasi tidak dapat dilakukan. Namun jika ada kata "bypass" maka operasi tetap dapat dijalankan.
```sh
if(strstr(path, "restricted")!=NULL){
        if(strstr(path, "bypass")!=NULL){
            createLog("SUCCESS", "UNLINK", action);
        }else{
            createLog("FAILED", "UNLINK", action);
            return 0;
        }
    } else{
        createLog("SUCCESS", "UNLINK", action);
    }
```
> Cuplikan code delete. Operasi rename juga memiliki struktur yang sama, hanya perbedaan UNLINK menjadi RENAME.

- Operasi selain rename dan delete akan selalu diperbolehkan sehingga cuplikan code sebelumnya diubah menjadi 
```sh
sprintf(action, "ACCESS file %s.", fpath);
createLog("SUCCESS", "READ", action);
```
> Cuplikan code read. 



### Soal 5
Setelah sukses menjadi pengusaha streaming musik di chapter kehidupan sebelumnya, Elshe direkrut oleh lembaga rahasia untuk membuat sistem rahasia yang terenkripsi.  Kalian perlu membantu Elshe dan membuat program rahasia.c. Pada program rahasia.c, terdapat beberapa hal yang harus kalian lakukan sebagai berikut.

- Program rahasia.c merupakan file FUSE yang akan digunakan untuk melakukan mount folder pada Docker Container. Unduh file  rahasia.zip kemudian lakukan unzip pada file rahasia.zip menjadi folder rahasia.
- Seperti soal a, folder rahasia akan di-mount pada Docker Container dengan image bernama  rahasia_di_docker_(Kode Kelompok) pada direktori /usr/share. Gunakan Ubuntu Focal Fossa sebagai base image pada Dockerfile.
- Setelah melakukan mount, buatlah register system yang menyimpan kredensial berupa username dan password. Agar lebih aman, password disimpan dengan menggunakan hashing MD5. Untuk mempermudah kalian, gunakan system() serta gunakan built-in program untuk melakukan hashing MD5 pada Linux (tidak wajib). Username dan password akan disimpan dengan format (username);(password). Tidak boleh ada user yang melakukan register dengan username yang sama. Kemudian, Buatlah login system yang mencocokkan kredensial antara username dan password.

> Unduh File
```sh
void download_file(const char *url, const char *outputPath){   

    char command[1000];
    sprintf(command, "wget \"%s\" -O \"%s\"", url, outputPath);
    system(command);
}
```
> Unzip File
```sh
//melakukan unzip file
void unzip_file(const char *outputPath){

    char command[256];
    //sekalian hapus file zip nya karena sudah tidak digunakan
    sprintf(command, "unzip -d . %s && rm rahasia.zip", outputPath);
    system(command);
}
```
> Duplicate folder rahasia yang telah di download menjadi folder tmp yang akan berisi folder rahasia. Untuk menjadi folder yang berisi rename file.
```sh
//copy folder ke /tmp karena dibuat untuk rename nantinya
void copyFolder(){
    system("mkdir -p tmp");

    system("cp -R rahasia tmp");
}

```
> Melakukan mount ke docker container. Mengisi volumes pada docker-compose yang akan berisi path folder rahasia (sebelum di rename) dan folder tmp (setelah direname). Menggunakan ubuntu focal fossa serta penamaan image adalah rahasia_di_docker_a01
```sh
  version: "3"
  services:
    myapp:
      build:
        context: .
        dockerfile: Dockerfile
      image: rahasia_di_docker_a01
      container_name: container_rahasia
      command: tail -f /dev/null
      volumes:
        - /home/dhe/Documents/Diajeng/Programming/sisop/Praktikum4/rahasia:/usr/before
        - /home/dhe/Documents/Diajeng/Programming/sisop/Praktikum4/tmp/rahasia:/usr/share
```
Isi Dockerfile
```sh
FROM ubuntu:focal

RUN mkdir -p /usr/share && mkdir -p /usr/before


# Tambahkan perintah-perintah instalasi atau konfigurasi tambahan jika diperlukan

CMD tail -f /dev/null
```
> Mengubah register system yang menyimpan username dan password lalu password menggunakan hashing MD5. Dan simpan kredensial ke local.
sebelum masuk register system
```sh
if(pid2 == 0){
    //hak akses default tidak akan berubah
    umask(0);

    //alamat dari struktur xmp_oper yang berisi implementasi dari operasi-operasi filesystem
    //NULL berarti tidak ada data tambahan yang diteruskan ke filesystem
    fuse_main(argc, argv, &xmp_oper, NULL);
}

else{
    
    //kita pull imagenya dulu lalu build docker-compose
    system("docker pull ubuntu:focal");
    system("docker-compose up -d");
```
Setelah build docker image dan container, lalu masuk ke register system
```sh
system("sudo touch users.txt");
system("sudo chmod 777 users.txt");

printf("Welcome to main menu!!\n");
printf("Please choose between login or register (input: 'login' or 'register'): ");

char temp[20];
scanf("%s", temp);

if (strcmp(temp, "register") == 0) {
    regis();
} 

else if(strcmp(temp, "login") == 0) {
    login();
}

else{

    while (strcmp(temp, "login") != 0 && strcmp(temp, "register") != 0) {

        printf("Invalid choice! Please enter either 'login' or 'register': ");
        scanf("%s", temp);
    }

    if (strcmp(temp, "login") == 0) {

        login();
    } 
    
    else if (strcmp(temp, "register") == 0) {
        regis();
    }
}
```
Sistem regis
```sh
//regis
void regis(){

    puts("Welcome to Register Menu");

    char username[100];
    printf("Enter your new username: ");
    scanf("%s", username);
    username[strcspn(username, "\n")] = '\0';
    
    //cek apakah user yang dimasukkan berbeda
    while (isUsernameAvail(username)) {
        
        printf("Username is already taken. Please enter a different username: ");
        scanf("%s", username);
        username[strcspn(username, "\n")] = '\0';
    }
    
    char password[100];
    printf("Enter your new password: ");
    char* input = getpass("");
    strncpy(password, input, 100);

    //hapus input yang tersimpan di memori
    memset(input, 0, strlen(input));

    //membuat jadi md5 hashing
    char hashed_password[500];
    strcpy(hashed_password, GetMD5String(password, sizeof(password)));

    //menyimpan ke users.txt
    registerUser(username, hashed_password);

    printf("Registration successful. Username and password have been saved.\n");

    char choice[20];
    printf("Choose an option 'login' or 'done' : ");
    scanf("%s", choice);

    if (strcmp(choice, "login") == 0) {
        login();
    }

};
```
Cek apakah username available atau tidak
```sh
//cek apakah user available
int isUsernameAvail(const char *username) {
    FILE *file = fopen("users.txt", "r");
    if (file == NULL) {
        printf("Failed to open users.txt\n");
        exit(1);
    }

    char line[100];
    while (fgets(line, sizeof(line), file)) {
        //cek apakah sama dengan username
        char *found = strstr(line, username);
        if (found != NULL) {
            fclose(file);
            return 1;
        }
    }

    fclose(file);

    return 0;
}
```
Mengubah password menjadi hashing MD5
```sh
//kode hashing md5 dari google
typedef union uwb {
	unsigned w;
	unsigned char b[4];
} MD5union;

typedef unsigned DigestArray[4];

unsigned func0(unsigned abcd[]) {
	return (abcd[1] & abcd[2]) | (~abcd[1] & abcd[3]);
}

unsigned func1(unsigned abcd[]) {
	return (abcd[3] & abcd[1]) | (~abcd[3] & abcd[2]);
}

unsigned func2(unsigned abcd[]) {
	return  abcd[1] ^ abcd[2] ^ abcd[3];
}

unsigned func3(unsigned abcd[]) {
	return abcd[2] ^ (abcd[1] | ~abcd[3]);
}

typedef unsigned(*DgstFctn)(unsigned a[]);

unsigned *calctable(unsigned *k)
{
	double s, pwr;
	int i;

	pwr = pow(2.0, 32);
	for (i = 0; i<64; i++) {
		s = fabs(sin(1.0 + i));
		k[i] = (unsigned)(s * pwr);
	}
	return k;
}

unsigned rol(unsigned r, short N)
{
	unsigned  mask1 = (1 << N) - 1;
	return ((r >> (32 - N)) & mask1) | ((r << N) & ~mask1);
}

unsigned* Algorithms_Hash_MD5(const char *msg, int mlen)
{
	static DigestArray h0 = { 0x67452301, 0xEFCDAB89, 0x98BADCFE, 0x10325476 };
	static DgstFctn ff[] = { &func0, &func1, &func2, &func3 };
	static short M[] = { 1, 5, 3, 7 };
	static short O[] = { 0, 1, 5, 0 };
	static short rot0[] = { 7, 12, 17, 22 };
	static short rot1[] = { 5, 9, 14, 20 };
	static short rot2[] = { 4, 11, 16, 23 };
	static short rot3[] = { 6, 10, 15, 21 };
	static short *rots[] = { rot0, rot1, rot2, rot3 };
	static unsigned kspace[64];
	static unsigned *k;

	static DigestArray h;
	DigestArray abcd;
	DgstFctn fctn;
	short m, o, g;
	unsigned f;
	short *rotn;
	union {
		unsigned w[16];
		char     b[64];
	}mm;
	int os = 0;
	int grp, grps, q, p;
	unsigned char *msg2;

	if (k == NULL) k = calctable(kspace);

	for (q = 0; q<4; q++) h[q] = h0[q];

	{
		grps = 1 + (mlen + 8) / 64;
		msg2 = (unsigned char*)malloc(64 * grps);
		memcpy(msg2, msg, mlen);
		msg2[mlen] = (unsigned char)0x80;
		q = mlen + 1;
		while (q < 64 * grps) { msg2[q] = 0; q++; }
		{
			MD5union u;
			u.w = 8 * mlen;
			q -= 8;
			memcpy(msg2 + q, &u.w, 4);
		}
	}

	for (grp = 0; grp<grps; grp++)
	{
		memcpy(mm.b, msg2 + os, 64);
		for (q = 0; q<4; q++) abcd[q] = h[q];
		for (p = 0; p<4; p++) {
			fctn = ff[p];
			rotn = rots[p];
			m = M[p]; o = O[p];
			for (q = 0; q<16; q++) {
				g = (m*q + o) % 16;
				f = abcd[1] + rol(abcd[0] + fctn(abcd) + k[q + 16 * p] + mm.w[g], rotn[q % 4]);

				abcd[0] = abcd[3];
				abcd[3] = abcd[2];
				abcd[2] = abcd[1];
				abcd[1] = f;
			}
		}
		for (p = 0; p<4; p++)
			h[p] += abcd[p];
		os += 64;
	}
	return h;
}

const char* GetMD5String(const char *msg, int mlen) {
	char str[33];
	strcpy(str, "");
	int j;
	unsigned *d = Algorithms_Hash_MD5(msg, strlen(msg));
	MD5union u;
	for (j = 0; j<4; j++) {
		u.w = d[j];
		char s[9];
		sprintf(s, "%02x%02x%02x%02x", u.b[0], u.b[1], u.b[2], u.b[3]);
		strcat(str, s);
	}

	return strdup(str);
}

```
Menyimpan username dan password baru ke user.txt dalam fungsi registerUser
```sh
//menyimpan user baru
void registerUser(const char *username, const char *password) {
    FILE *file = fopen("users.txt", "a");
    if (file == NULL) {
        printf("Failed to open users.txt\n");
        exit(1);
    }

    fprintf(file, "%s;%s\n", username, password);
    fclose(file);
}
```

> Sistem login
```sh
//login
void login(){
    
    puts("Welcome to Login Menu");

    char username[100];
    char password[100];
    char hashed_password[500];

    int isAuthenticated = 0;

    do {
        printf("Enter your username: ");
        scanf("%s", username);
        username[strcspn(username, "\n")] = '\0';
        
        printf("Enter your new password: ");
        //agar tidak muncul diterminal
        char* input = getpass("");
        strncpy(password, input, 100);

        //hapus input yang tersimpan di memori
        memset(input, 0, strlen(input));

        //menyimpan hasil md5 ke hashed_password
        strcpy(hashed_password, GetMD5String(password, sizeof(password)));

        if (checkCredentials(username, hashed_password)) {
            isAuthenticated = 1;
            printf("Login successful!\n");

            akses();
        } 
        else {
            printf("Invalid username or password. Please try again.\n");
        }
    } 
    while (!isAuthenticated);


};
```
Cek username dan password apakah sesuai dalam users.txt
```sh
//cek credential untuk login dan register

int checkCredentials(const char* username, const char* password) {
    FILE* file = fopen("users.txt", "r");
    if (file == NULL) {
        printf("Failed to open users.txt.\n");
        return 0;
    }

    char line[100];
    char storedUsername[100];
    char storedPassword[500];
    int isAuthenticated = 0;

    while (fgets(line, sizeof(line), file)) {
        //cek apakah username dan password sesuai dalam users.txt
        sscanf(line, "%[^;];%s", storedUsername, storedPassword);
        if (strcmp(username, storedUsername) == 0 && strcmp(password, storedPassword) == 0) {
            isAuthenticated = 1;
            break;
        }
    }

    fclose(file);
    return isAuthenticated;
}
```
Jika login berhasil, maka user dapat melakukan berbagai akses yaitu akses read (sebelum di rename) dan akses rename (semua folder isinya telah di rename)
```sh
//hal yang didapatkan user saat sudah login
void akses(){

    char jawaban[20];
    while (1) {
        printf("Hello, What do you want to do? (Input: 'read' or 'rename' or 'exit'): ");
        scanf("%s", jawaban);

        //dapat langsung memunculkan isi dari folder rahasia yang disimpan dalam /usr/before
        if (strcmp(jawaban, "read") == 0) {
            system("docker exec -it container_rahasia /bin/bash -c 'ls /usr/before'");
        } 

        //memunculkan isi dari folder rahasia yang telah direname dan tersimpan dalam /usr/share
        else if (strcmp(jawaban, "rename") == 0) {
            system("docker exec -it container_rahasia /bin/bash -c 'ls /usr/share'");  
        } 

        else if(strcmp(jawaban, "exit") == 0){
            break;
        }
        
        else {
            printf("Invalid choice. Please try again.\n");
        }
    }
}
```
Untuk melakukan rename, gunakan fuse. Disini, diubah melalui xmp_readdir
```sh
//read directory
static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi) {
    char fpath[1000];

    if (strcmp(path, "/") == 0) {
        path = dirpath;
        sprintf(fpath, "%s", path);
    } 
    
    else sprintf(fpath, "%s%s", dirpath, path);

    // printf("Debug: Calling readdir for path: %s\n", fpath); // Menampilkan path yang sedang diproses

    int res = 0;

    DIR *dp;
    //informasi tentang entri dalam directory
    struct dirent *de;

    //memberikan peringatan terkait variabel yang tidak digunakan pada kompilasi
    (void)offset;
    (void)fi;

    dp = opendir(fpath);

    if (dp == NULL) {
        printf("Debug: Failed to open directory: %s\n", fpath); // Menampilkan pesan jika gagal membuka direktori
        return -errno;
    }

    //membaca setiap entry dalam dp ke dirent de
    while ((de = readdir(dp)) != NULL) {
        //menyimpan atribut dari entri dirent yang sedang dibaca
        struct stat st;
        //seluruh byte dibuat 0
        memset(&st, 0, sizeof(st));

        //inode dari st berisi inode dari de
        st.st_ino = de->d_ino;
        //mengambil tipe file 
        st.st_mode = de->d_type << 12;

        char entry_path[5000];
        sprintf(entry_path, "%s/%s", fpath, de->d_name);

        //memeriksa status file untuk mendapatkan informasi
        if (stat(entry_path, &st) == -1) {
            // printf("Debug: Failed to get file information for: %s\n", entry_path); // Menampilkan pesan jika gagal mendapatkan informasi berkas
            return -errno;
        }
        
        char new_entry_path[5000];
        // cek apakah termasuk directory
        if (S_ISDIR(st.st_mode)) {

            //memeriksa apakah directory yang sedang diproses bukanlah directory saat ini atau directory induk
            if (strcmp(de->d_name, ".") != 0 && strcmp(de->d_name, "..") != 0) {

                char new_name[2000];
                sprintf(new_name, "%s_A01", de->d_name);

                // Mengecek apakah kata 'A01' sudah ada dalam nama folder sebelum melakukan rename
                if (strstr(de->d_name, "A01") == NULL) {
                    // Melakukan rename folder
                    sprintf(entry_path, "%s/%s", fpath, de->d_name);
                    sprintf(new_entry_path, "%s/%s", fpath, new_name);
                    if (rename(entry_path, new_entry_path) == -1) {
                        // printf("Debug: Failed to rename folder: %s\n", entry_path);
                    }

                    res = xmp_readdir(entry_path, buf, filler, offset, fi);
                }
            }

        }
        //cek apakah termasuk file
        if(S_ISREG(st.st_mode)) {
            
            //apakah nama file mengandung "A01"
            if (strstr(de->d_name, "A01") == NULL) {
                char new_name[2000];
                //pointer ke titik terakhir. jadi ext isinya bakal .txt or sth else
                char *ext = strrchr(de->d_name, '.');
                if (ext != NULL) {
                    // *ext = '\0'; // Remove the file extension from the name
                    sprintf(new_name, "A01_%s", de->d_name);

                    char new_entry_path[5000];
                    char old_entry_path[5000];
                    sprintf(old_entry_path, "%s/%s", fpath, de->d_name);

                    sprintf(new_entry_path, "%s/%s", fpath, new_name);

                    // Memeriksa keberadaan file sebelum melakukan rename
                    if (access(new_entry_path, F_OK) == -1) {
                        // File dengan nama yang diinginkan belum ada, lanjutkan dengan proses rename
                        if (rename(entry_path, new_entry_path) == -1) {
                            // printf("Debug: Failed to rename file: %s\n", entry_path);
                            perror("Error"); // Menampilkan pesan error lebih spesifik
                        }
                    } else {
                        // printf("Debug: File with desired name already exists: %s\n", new_entry_path);
                        // Tambahkan log atau penanganan kesalahan sesuai kebutuhan
                    }
                }    
            }
        }

        //mengisi buffer dan memastikan buffer tidak penuh
        /**
         *      buf: Buffer yang digunakan untuk menyimpan informasi mengenai entri dirent.
                de->d_name: Nama dari entri dirent yang sedang dibaca.
                &st: Alamat dari struktur st yang berisi atribut (stat) dari entri dirent tersebut.
                0: Nilai offset yang digunakan untuk menentukan posisi entri dirent dalam buffer. Dalam hal ini, nilai 0 menunjukkan bahwa entri dirent akan dimasukkan ke dalam buffer pada posisi saat ini.
        */

        res = (filler(buf, de->d_name, &st, 0));
        if (res != 0) break;
    }

    closedir(dp);

    // printf("Debug: readdir completed for path: %s\n", fpath); // Menampilkan pesan setelah selesai membaca direktori

    return 0;
}
```
Fungsi xmp_rename untuk melakukan rename
```sh
//melakukan rename
static int xmp_rename(const char *from, const char *to)
{
    char fpathFrom[1000];
    char fpathTo[1000];

    sprintf(fpathFrom, "%s%s", dirpath, from);
    sprintf(fpathTo, "%s%s", dirpath, to);

    printf("rename: %s to %s\n", fpathFrom, fpathTo);

    //terjadi rename
    int res = rename(fpathFrom, fpathTo);
    if (res == -1)
        return -errno;

    return 0;
}
```
Masukkan fungsi xmp_rename ke fuse_operations
```sh
//menentukan operasi-operasi yang akan dilakukan pada filesystem yang sedang dibuat
static struct fuse_operations xmp_oper = {
    //mendapatkan atribut (metadata) dari file atau directory
    .getattr = xmp_getattr,
    //membaca isi dari sebuah direktori
    .readdir = xmp_readdir,
    //membaca isi dari sebuah file
    .read = xmp_read,
    //melakukan rename
    .rename = xmp_rename,
    //untuk membuat tree
    .open = xmp_open,
};
```
> Lakukan list seluruh folder, subfolder, dan file yang telah di rename dalam file result.txt dengan menggunakan tree
Gunakan xmp_open
```sh
//mount path ini digunakan untuk menjalankan command tree
static  const  char *mount_path = "/home/dhe/Documents/Diajeng/Programming/sisop/Praktikum4/tmp/rahasia";

static int xmp_open(const char *path, struct fuse_file_info *fi)
{
    (void) fi;

    // Buka file result.txt untuk dituliskan output dari perintah "tree"
    /**
     * status:
     * O_WRONLY: mode write only
     * O_CREAT: file akan dibuat jika belum ada
     * O_TRUNC: file akan dipotong (truncate) jika sudah ada sebelumnya
     * 0664: hak akses read write untuk pemilik file
    */
    int fd = open("/home/dhe/Documents/Diajeng/Programming/sisop/Praktikum4/result.txt", O_WRONLY | O_CREAT | O_TRUNC, 0644);
    if (fd == -1) {
        perror("Error opening file");
        return -1;
    }

    // Menyimpan stdout yang sebelumnya digunakan untuk mencetak ke konsol
    int simpan_stdout = dup(STDOUT_FILENO);

    // Mengarahkan stdout ke file result.txt
    if (dup2(fd, STDOUT_FILENO) == -1) {
        perror("Error redirecting stdout");
        close(fd);
        return -1;
    }

    // Menjalankan perintah "tree" pada direktori yang telah dimount
    char command[1024];
    snprintf(command, sizeof(command), "tree %s", mount_path);

    system(command);

    // Mengembalikan stdout ke keadaan semula
    dup2(simpan_stdout, STDOUT_FILENO);
    close(simpan_stdout);

    close(fd);

    return 0;
}
```
Masukkan xmp_open ke fuse_operations
```sh
//menentukan operasi-operasi yang akan dilakukan pada filesystem yang sedang dibuat
static struct fuse_operations xmp_oper = {
    //mendapatkan atribut (metadata) dari file atau directory
    .getattr = xmp_getattr,
    //membaca isi dari sebuah direktori
    .readdir = xmp_readdir,
    //membaca isi dari sebuah file
    .read = xmp_read,
    //melakukan rename
    .rename = xmp_rename,
    //untuk membuat tree
    .open = xmp_open,
};
```
> Hitung jumlah file berdasarkan extension
Di dalam parent process, kita lakukan countEntities ke path folder rahasia (yang belum direname)
```sh
if(pid == 0){ //child process

    download_file(url, outputPath);
}
else{

    wait(NULL);

    //karena folder rahasia gabisa kesimpen kalau ada compiler bernama rahasia
    remove("rahasia");

    //melakukan unzip
    unzip_file(outputPath);

    //melakukan copy folder karena untuk rename
    copyFolder();

    //menghitung jumlah file dan folder
    const char *pathh = "/home/dhe/Documents/Diajeng/Programming/sisop/Praktikum4/rahasia";
    countEntities(pathh);

```
```sh
int countfolder = 0;
int countjpg = 0;
int countpdf = 0;
int counttxt= 0;
int countmp3 = 0;
int countpng = 0;
int countdocx = 0;
int countgif = 0;

//untuk rekursi menghitung banyaknya folder dan file
void countEntities(const char *pathh) {
    DIR *dir = opendir(pathh);
    if (dir == NULL) {
        printf("Failed to open directory: %s\n", pathh);
        return;
    }

    struct dirent *entry;
    //mengambil file dalam pathh
    while ((entry = readdir(dir)) != NULL) {
        // Skip "." and ".." entries
        if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0) {
            continue;
        }

        // Construct the full path of the entry
        char entryPath[1000];
        snprintf(entryPath, sizeof(entryPath), "%s/%s", pathh, entry->d_name);

        struct stat st;
        if (stat(entryPath, &st) == -1) {
            printf("Failed to get file information for: %s\n", entryPath);
            continue;
        }

        //cek apakah merupakan file
        if (S_ISREG(st.st_mode)) {
            if (strstr(entry->d_name, "pdf") != NULL) {
                countpdf++;
            } 
            
            else if (strstr(entry->d_name, "jpg") != NULL) {
                countjpg++;
            } 
            
            else if (strstr(entry->d_name, "txt") != NULL) {
                counttxt++;
            } 
            
            else if (strstr(entry->d_name, "mp3") != NULL) {
                countmp3++;
            } 
            
            else if (strstr(entry->d_name, "png") != NULL) {
                countpng++;
            } 
            
            else if (strstr(entry->d_name, "docx") != NULL) {
                countdocx++;
            } 
            
            else if (strstr(entry->d_name, "gif") != NULL) {
                countgif++;
            } 
            
            else {
                // Extension tidak sesuai dengan yang diharapkan, tambahkan penanganan kesalahan di sini
                printf("Extension tidak dikenali: %s\n", entry->d_name);
            }
        }

        //cek apakah merupakan directory
        else if (S_ISDIR(st.st_mode)) {
            countfolder++;

            // akan terjadi rekursi karena membaca isi dari subfolder
            countEntities(entryPath);
        }
    }

    closedir(dir);
}
```
Simpan hasilnya ke extension.txt
```sh
const char *pathh = "/home/dhe/Documents/Diajeng/Programming/sisop/Praktikum4/rahasia";
countEntities(pathh);

//menyimpan tree
system("sudo touch extension.txt");
system("sudo chmod 777 extension.txt");

FILE *file = fopen("extension.txt", "w");
if (file == NULL) {
    printf("Failed to open file.\n");
    return 1;
}

fprintf(file, "pdf = %d\n", countpdf);
fprintf(file, "jpg = %d\n", countjpg);
fprintf(file, "folder = %d\n", countfolder);
fprintf(file, "txt = %d\n", counttxt);
fprintf(file, "mp3 = %d\n", countmp3);
fprintf(file, "png = %d\n", countpng);
fprintf(file, "docx = %d\n", countdocx);
fprintf(file, "gif = %d\n", countgif);

fclose(file);
```

> output


Hasil tree dalam result.txt sesuai
```sh
/home/dhe/Documents/Diajeng/Programming/sisop/Praktikum4/tmp/rahasia
├── 80T86jn7xh_A01
│   ├── A01_elaine-casap-82gJggDId-U-unsplash.jpg
│   └── A01_tobias-tullius-3BcSyNMJ2WM-unsplash.jpg
├── A01_b.pdf
├── A01_carles-rabada-kMde0v9tYYM-unsplash.jpg
├── A01_File-49ynD.txt
├── A01_File-Oadk3.txt
├── A01_File-PdCim.txt
├── A01_funny-eastern-short-music-vlog-background-hip-hop-beat-29-sec-148905.mp3
├── A01_mygamer.gif
├── A01_png-transparent-construction-equipment-illustrations-heavy-machinery-architectural-engineering-truck-vehicle-construction-sites-icon-material-free-free-logo-design-template-building-text-thumbnail.png
├── AG5zGHsasr_A01
│   └── A01_File-iVVm9.txt
├── dU87gUYIBN_A01
│   ├── A01_cinematic-logo-strong-and-wild-142807.mp3
│   ├── A01_clapping-opener-rhythmic-original-dynamic-promo-114789.mp3
│   ├── A01_mohamed-ahsan-cjkgrFt_6bE-unsplash.jpg
│   ├── A01_motivational-corporate-short-110681.mp3
│   ├── A01_png-transparent-free-download-icon-cloud-down-computer-system-blue-computer-blue-laptop-thumbnail.png
│   └── A01_sergey-zhesterev-lutEBBTpkeE-unsplash.jpg
├── Dyy2q9B0Ax_A01
│   ├── A01_File-CxD3o.txt
│   └── A01_patrick-hendry-jS0ysot7MwE-unsplash.jpg
├── FH2CQo5o8z_A01
│   ├── A01_File-dUnQq.txt
│   ├── A01_File-QDJhp.txt
│   ├── A01_File-uuXxv.txt
│   ├── A01_png-transparent-zombie-download-with-transparent-background-fantasy-free-thumbnail.png
│   └── A01_randall-ruiz-LVnJlyfa7Zk-unsplash.jpg
├── gGJ5r0Df82_A01
│   ├── A01_andre-tan-3SK2x-kbMSI-unsplash.jpg
│   ├── A01_andrii-ganzevych-IVlVHXgvi0Y-unsplash.jpg
│   ├── A01_carles-rabada-L7J0q5N7LjQ-unsplash.jpg
│   ├── A01_File-aBxnM.txt
│   ├── A01_File-EBdiV.txt
│   ├── A01_File-OlZ8w.txt
│   └── A01_File-XZJco.txt
├── IKJHSAU9731289dsa_A01
│   └── A01_png-transparent-mobile-phone-repair-material-mobile-phone-repair-phone-iphone-thumbnail.png
├── IzvxA5vw2U_A01
│   ├── A01_kris-mikael-krister-aGihPIbrtVE-unsplash.jpg
│   ├── A01_png-transparent-video-production-freemake-video-er-video-icon-free-angle-text-rectangle-thumbnail.png
│   └── A01_the-devilx27s-entry-143138.mp3
├── m4xOZ1lmux_A01
│   ├── A01_bharat-patil-_kPuQcU8C-A-unsplash.jpg
│   ├── A01_File-4qjlL.txt
│   ├── A01_File-uNOZN.txt
│   ├── A01_jon-tyson-Fz2BWfLDvGI-unsplash.jpg
│   └── A01_png-transparent-yellow-fire-fire-flame-fire-photography-orange-flame-thumbnail.png
├── mBFLP3XQMv_A01
│   ├── A01_epicaly-short-113909.mp3
│   ├── A01_File-6tNX4.txt
│   ├── A01_jcob-nasyr-uGPBqF1Yls0-unsplash.jpg
│   ├── A01_j.docx
│   ├── A01_ramona-flwrs-uJjhnN2WQJs-unsplash.jpg
│   ├── A01_redd-f-t8ts5bNQyWo-unsplash.jpg
│   └── A01_sami-takarautio-JiqalEW6Ml0-unsplash.jpg
├── nkSVqqpV7o_A01
│   ├── A01_c.pdf
│   ├── A01_d.pdf
│   ├── A01_epic-sport-clap-ampampamp-loop-main-9900.mp3
│   ├── A01_File-7Yf0S.txt
│   ├── A01_File-p6ZH2.txt
│   ├── A01_File-xBUoT.txt
│   ├── A01_k-on-yui-hirasawa.gif
│   ├── A01_matthew-feeney-L35x5fU07hY-unsplash.jpg
│   ├── A01_simple-logo-149190.mp3
│   ├── A01_thomas-owen-Y8vui_5mdto-unsplash.jpg
│   └── A01_upbeat-future-bass-opener-136069.mp3
├── Pp9LXs5FkR_A01
│   ├── A01_clapping-music-for-typographic-video-version-2-112975.mp3
│   ├── A01_File-UWKAJ.txt
│   ├── A01_File-X8lyf.txt
│   ├── A01_honkai-impact3rd-cute.gif
│   └── A01_wexor-tmg-L-2p8fapOA8-unsplash.jpg
├── SaEixBbm6y_A01
│   ├── A01_g.pdf
│   ├── A01_haikyuu-anime.gif
│   └── A01_h.pdf
├── uuXVhCbP3X_A01
│   ├── A01_a.pdf
│   ├── A01_e.pdf
│   ├── A01_epic-background-music-for-video-dramatic-hip-hop-24-seconds-149629.mp3
│   ├── A01_png-transparent-blue-dna-illustration-dna-3d-rendering-3d-computer-graphics-free-to-pull-dna-material-3d-computer-graphics-free-logo-design-template-photography-thumbnail.png
│   └── A01_png-transparent-tableware-plate-white-ceramic-plate-platter-dishware-download-with-transparent-background-thumbnail.png
├── VOpdACXtF3_A01
│   ├── A01_heart-ritsu-tainaka.gif
│   ├── A01_png-transparent-moon-moon-image-file-formats-nature-moon-png-thumbnail.png
│   └── A01_png-transparent-snowflake-light-snowflake-free-glass-free-logo-design-template-symmetry-thumbnail.png
├── vZAZecJwKF_A01
│   ├── A01_i.docx
│   ├── A01_png-transparent-kiwifruit-kiwi-free-fruit-kiwi-s-watercolor-painting-image-file-formats-food-thumbnail.png
│   ├── A01_png-transparent-sun-the-sun-sunscreen-light-sphere-sun-image-file-formats-orange-sphere-thumbnail.png
│   └── A01_tango-background-hip-hop-music-for-video-29-seconds-149879.mp3
├── xP4UcxRZE5_A01
│   ├── A01_energetic-background-reggaeton-short-music-27-sec-fun-vlog-music-149384.mp3
│   ├── A01_f.pdf
│   ├── A01_fun-background-hip-hop-short-music-27-sec-energetic-vlog-music-148916.mp3
│   ├── A01_kiss-hug.gif
│   ├── A01_png-transparent-pearl-material-sphere-pearl-balloon-download-with-transparent-background-free-thumbnail.png
│   ├── A01_png-transparent-yellow-fireworks-illustration-fireworks-pyrotechnics-free-to-pull-the-material-fireworks-s-free-logo-design-template-holidays-poster-thumbnail.png
│   └── A01_sirin-honkai-impact.gif
└── ZlzqBmSYbE_A01
    ├── A01_File-nENTg.txt
    ├── A01_File-NWnoK.txt
    ├── A01_File-xnlpH.txt
    ├── A01_png-transparent-bubble-illustration-water-drop-rain-drops-angle-white-text-thumbnail.png
    └── A01_png-transparent-golden-pistol-guns-pistol-free-download-pistol-firearms-thumbnail.png

18 directories, 90 files
```
