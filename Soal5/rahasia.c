#define FUSE_USE_VERSION 28
#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <sys/time.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <math.h>
#include <openssl/md5.h>

//download file
void download_file(const char *url, const char *outputPath){   

    char command[1000];
    sprintf(command, "wget \"%s\" -O \"%s\"", url, outputPath);
    system(command);
}

//melakukan unzip file
void unzip_file(const char *outputPath){

    char command[256];
    //sekalian hapus file zip nya karena sudah tidak digunakan
    sprintf(command, "unzip -d . %s && rm rahasia.zip", outputPath);
    system(command);
}

//copy folder ke /tmp karena dibuat untuk rename nantinya
void copyFolder(){
    system("mkdir -p tmp");

    system("cp -R rahasia tmp");
}

//hasil dari copyFolder()
static  const  char *dirpath = "/home/dhe/Documents/Diajeng/Programming/sisop/Praktikum4/tmp/rahasia";

// Mendapatkan atribut file atau directory
static int xmp_getattr(const char *path, struct stat *stbuf){
    int res;
    char fpath[1000];

    sprintf(fpath, "%s%s", dirpath, path);

    // printf("getattr: %s\n", fpath);

    // Menggunakan lstat untuk mendapatkan atribut file/direktori
    // hasil atribut disimpan di stbuf
    res = lstat(fpath, stbuf);
    if (res == -1)
        return -errno;

    return 0;
}


// Fungsi untuk membaca isi direktori
/**
 * Buf: pointer ke buffer yang akan digunakan untuk mengisi entri direktori
 * filler: fungsi callback untuk memasukkan entri direktori ke dalam buffer
 * offset: posisi awal bacaan
 * fi: pointer ke struktur fuse_file_info
*/

//read directory
static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi) {
    char fpath[1000];

    if (strcmp(path, "/") == 0) {
        path = dirpath;
        sprintf(fpath, "%s", path);
    } 
    
    else sprintf(fpath, "%s%s", dirpath, path);

    // printf("Debug: Calling readdir for path: %s\n", fpath); // Menampilkan path yang sedang diproses

    int res = 0;

    DIR *dp;
    //informasi tentang entri dalam directory
    struct dirent *de;

    //memberikan peringatan terkait variabel yang tidak digunakan pada kompilasi
    (void)offset;
    (void)fi;

    dp = opendir(fpath);

    if (dp == NULL) {
        printf("Debug: Failed to open directory: %s\n", fpath); // Menampilkan pesan jika gagal membuka direktori
        return -errno;
    }

    //membaca setiap entry dalam dp ke dirent de
    while ((de = readdir(dp)) != NULL) {
        //menyimpan atribut dari entri dirent yang sedang dibaca
        struct stat st;
        //seluruh byte dibuat 0
        memset(&st, 0, sizeof(st));

        //inode dari st berisi inode dari de
        st.st_ino = de->d_ino;
        //mengambil tipe file 
        st.st_mode = de->d_type << 12;

        char entry_path[5000];
        sprintf(entry_path, "%s/%s", fpath, de->d_name);

        //memeriksa status file untuk mendapatkan informasi
        if (stat(entry_path, &st) == -1) {
            // printf("Debug: Failed to get file information for: %s\n", entry_path); // Menampilkan pesan jika gagal mendapatkan informasi berkas
            return -errno;
        }
        
        char new_entry_path[5000];
        // cek apakah termasuk directory
        if (S_ISDIR(st.st_mode)) {

            //memeriksa apakah directory yang sedang diproses bukanlah directory saat ini atau directory induk
            if (strcmp(de->d_name, ".") != 0 && strcmp(de->d_name, "..") != 0) {

                char new_name[2000];
                sprintf(new_name, "%s_A01", de->d_name);

                // Mengecek apakah kata 'A01' sudah ada dalam nama folder sebelum melakukan rename
                if (strstr(de->d_name, "A01") == NULL) {
                    // Melakukan rename folder
                    sprintf(entry_path, "%s/%s", fpath, de->d_name);
                    sprintf(new_entry_path, "%s/%s", fpath, new_name);
                    if (rename(entry_path, new_entry_path) == -1) {
                        // printf("Debug: Failed to rename folder: %s\n", entry_path);
                    }

                    res = xmp_readdir(entry_path, buf, filler, offset, fi);
                }
            }

        }
        //cek apakah termasuk file
        if(S_ISREG(st.st_mode)) {
            
            //apakah nama file mengandung "A01"
            if (strstr(de->d_name, "A01") == NULL) {
                char new_name[2000];
                //pointer ke titik terakhir. jadi ext isinya bakal .txt or sth else
                char *ext = strrchr(de->d_name, '.');
                if (ext != NULL) {
                    // *ext = '\0'; // Remove the file extension from the name
                    sprintf(new_name, "A01_%s", de->d_name);

                    char new_entry_path[5000];
                    char old_entry_path[5000];
                    sprintf(old_entry_path, "%s/%s", fpath, de->d_name);

                    sprintf(new_entry_path, "%s/%s", fpath, new_name);

                    // Memeriksa keberadaan file sebelum melakukan rename
                    if (access(new_entry_path, F_OK) == -1) {
                        // File dengan nama yang diinginkan belum ada, lanjutkan dengan proses rename
                        if (rename(entry_path, new_entry_path) == -1) {
                            // printf("Debug: Failed to rename file: %s\n", entry_path);
                            perror("Error"); // Menampilkan pesan error lebih spesifik
                        }
                    } else {
                        // printf("Debug: File with desired name already exists: %s\n", new_entry_path);
                        // Tambahkan log atau penanganan kesalahan sesuai kebutuhan
                    }
                }    
            }
        }

        //mengisi buffer dan memastikan buffer tidak penuh
        /**
         *      buf: Buffer yang digunakan untuk menyimpan informasi mengenai entri dirent.
                de->d_name: Nama dari entri dirent yang sedang dibaca.
                &st: Alamat dari struktur st yang berisi atribut (stat) dari entri dirent tersebut.
                0: Nilai offset yang digunakan untuk menentukan posisi entri dirent dalam buffer. Dalam hal ini, nilai 0 menunjukkan bahwa entri dirent akan dimasukkan ke dalam buffer pada posisi saat ini.
        */

        res = (filler(buf, de->d_name, &st, 0));
        if (res != 0) break;
    }

    closedir(dp);

    // printf("Debug: readdir completed for path: %s\n", fpath); // Menampilkan pesan setelah selesai membaca direktori

    return 0;
}

//read data dari openfile
static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi) {
    char fpath[1000];

    if (strcmp(path, "/") == 0) {
        path = dirpath;
        sprintf(fpath, "%s", path);
    } 
    else sprintf(fpath, "%s%s", dirpath, path);

    // printf("Debug: Calling read for file: %s\n", fpath); // Menampilkan path file yang sedang dibaca

    int res = 0;
    int fd = 0;

    (void)fi;

    //open fpath dengan aturan read and write
    fd = open(fpath, O_RDWR);

    if (fd == -1) {
        // printf("Debug: Failed to open file: %s\n", fpath); // Menampilkan pesan jika gagal membuka file
        return -errno;
    }

    //membaca isi file
    /**
     * Fungsi ini menerima file descriptor (fd), buffer (buf), 
     * ukuran data yang akan dibaca (size), dan offset (offset) sebagai parameter. 
     * pread membaca data dari file yang 
     * diindikasikan oleh fd dengan ukuran size dan memulai dari offset yang ditentukan. 
     * Hasil pembacaan akan disimpan di dalam buf.
    */

    res = pread(fd, buf, size, offset);

    if (res == -1) {
        // printf("Debug: Failed to read file: %s\n", fpath); // Menampilkan pesan jika gagal membaca file
        res = -errno;
    }

    close(fd);

    // printf("Debug: read completed for file: %s\n", fpath); // Menampilkan pesan setelah selesai membaca file

    return res;
}

//melakukan rename
static int xmp_rename(const char *from, const char *to)
{
    char fpathFrom[1000];
    char fpathTo[1000];

    sprintf(fpathFrom, "%s%s", dirpath, from);
    sprintf(fpathTo, "%s%s", dirpath, to);

    printf("rename: %s to %s\n", fpathFrom, fpathTo);

    //terjadi rename
    int res = rename(fpathFrom, fpathTo);
    if (res == -1)
        return -errno;

    return 0;
}

//mount path ini digunakan untuk menjalankan command tree
static  const  char *mount_path = "/home/dhe/Documents/Diajeng/Programming/sisop/Praktikum4/tmp/rahasia";

static int xmp_open(const char *path, struct fuse_file_info *fi)
{
    (void) fi;

    // Buka file result.txt untuk dituliskan output dari perintah "tree"
    /**
     * status:
     * O_WRONLY: mode write only
     * O_CREAT: file akan dibuat jika belum ada
     * O_TRUNC: file akan dipotong (truncate) jika sudah ada sebelumnya
     * 0664: hak akses read write untuk pemilik file
    */
    int fd = open("/home/dhe/Documents/Diajeng/Programming/sisop/Praktikum4/result.txt", O_WRONLY | O_CREAT | O_TRUNC, 0644);
    if (fd == -1) {
        perror("Error opening file");
        return -1;
    }

    // Menyimpan stdout yang sebelumnya digunakan untuk mencetak ke konsol
    int simpan_stdout = dup(STDOUT_FILENO);

    // Mengarahkan stdout ke file result.txt
    if (dup2(fd, STDOUT_FILENO) == -1) {
        perror("Error redirecting stdout");
        close(fd);
        return -1;
    }

    // Menjalankan perintah "tree" pada direktori yang telah dimount
    char command[1024];
    snprintf(command, sizeof(command), "tree %s", mount_path);

    system(command);

    // Mengembalikan stdout ke keadaan semula
    dup2(simpan_stdout, STDOUT_FILENO);
    close(simpan_stdout);

    close(fd);

    return 0;
}

//menentukan operasi-operasi yang akan dilakukan pada filesystem yang sedang dibuat
static struct fuse_operations xmp_oper = {
    //mendapatkan atribut (metadata) dari file atau directory
    .getattr = xmp_getattr,
    //membaca isi dari sebuah direktori
    .readdir = xmp_readdir,
    //membaca isi dari sebuah file
    .read = xmp_read,
    //melakukan rename
    .rename = xmp_rename,
    //untuk membuat tree
    .open = xmp_open,
};

//kode hashing md5 dari google
typedef union uwb {
	unsigned w;
	unsigned char b[4];
} MD5union;

typedef unsigned DigestArray[4];

unsigned func0(unsigned abcd[]) {
	return (abcd[1] & abcd[2]) | (~abcd[1] & abcd[3]);
}

unsigned func1(unsigned abcd[]) {
	return (abcd[3] & abcd[1]) | (~abcd[3] & abcd[2]);
}

unsigned func2(unsigned abcd[]) {
	return  abcd[1] ^ abcd[2] ^ abcd[3];
}

unsigned func3(unsigned abcd[]) {
	return abcd[2] ^ (abcd[1] | ~abcd[3]);
}

typedef unsigned(*DgstFctn)(unsigned a[]);

unsigned *calctable(unsigned *k)
{
	double s, pwr;
	int i;

	pwr = pow(2.0, 32);
	for (i = 0; i<64; i++) {
		s = fabs(sin(1.0 + i));
		k[i] = (unsigned)(s * pwr);
	}
	return k;
}

unsigned rol(unsigned r, short N)
{
	unsigned  mask1 = (1 << N) - 1;
	return ((r >> (32 - N)) & mask1) | ((r << N) & ~mask1);
}

unsigned* Algorithms_Hash_MD5(const char *msg, int mlen)
{
	static DigestArray h0 = { 0x67452301, 0xEFCDAB89, 0x98BADCFE, 0x10325476 };
	static DgstFctn ff[] = { &func0, &func1, &func2, &func3 };
	static short M[] = { 1, 5, 3, 7 };
	static short O[] = { 0, 1, 5, 0 };
	static short rot0[] = { 7, 12, 17, 22 };
	static short rot1[] = { 5, 9, 14, 20 };
	static short rot2[] = { 4, 11, 16, 23 };
	static short rot3[] = { 6, 10, 15, 21 };
	static short *rots[] = { rot0, rot1, rot2, rot3 };
	static unsigned kspace[64];
	static unsigned *k;

	static DigestArray h;
	DigestArray abcd;
	DgstFctn fctn;
	short m, o, g;
	unsigned f;
	short *rotn;
	union {
		unsigned w[16];
		char     b[64];
	}mm;
	int os = 0;
	int grp, grps, q, p;
	unsigned char *msg2;

	if (k == NULL) k = calctable(kspace);

	for (q = 0; q<4; q++) h[q] = h0[q];

	{
		grps = 1 + (mlen + 8) / 64;
		msg2 = (unsigned char*)malloc(64 * grps);
		memcpy(msg2, msg, mlen);
		msg2[mlen] = (unsigned char)0x80;
		q = mlen + 1;
		while (q < 64 * grps) { msg2[q] = 0; q++; }
		{
			MD5union u;
			u.w = 8 * mlen;
			q -= 8;
			memcpy(msg2 + q, &u.w, 4);
		}
	}

	for (grp = 0; grp<grps; grp++)
	{
		memcpy(mm.b, msg2 + os, 64);
		for (q = 0; q<4; q++) abcd[q] = h[q];
		for (p = 0; p<4; p++) {
			fctn = ff[p];
			rotn = rots[p];
			m = M[p]; o = O[p];
			for (q = 0; q<16; q++) {
				g = (m*q + o) % 16;
				f = abcd[1] + rol(abcd[0] + fctn(abcd) + k[q + 16 * p] + mm.w[g], rotn[q % 4]);

				abcd[0] = abcd[3];
				abcd[3] = abcd[2];
				abcd[2] = abcd[1];
				abcd[1] = f;
			}
		}
		for (p = 0; p<4; p++)
			h[p] += abcd[p];
		os += 64;
	}
	return h;
}

const char* GetMD5String(const char *msg, int mlen) {
	char str[33];
	strcpy(str, "");
	int j;
	unsigned *d = Algorithms_Hash_MD5(msg, strlen(msg));
	MD5union u;
	for (j = 0; j<4; j++) {
		u.w = d[j];
		char s[9];
		sprintf(s, "%02x%02x%02x%02x", u.b[0], u.b[1], u.b[2], u.b[3]);
		strcat(str, s);
	}

	return strdup(str);
}

//cek credential untuk login dan register

int checkCredentials(const char* username, const char* password) {
    FILE* file = fopen("users.txt", "r");
    if (file == NULL) {
        printf("Failed to open users.txt.\n");
        return 0;
    }

    char line[100];
    char storedUsername[100];
    char storedPassword[500];
    int isAuthenticated = 0;

    while (fgets(line, sizeof(line), file)) {
        //cek apakah username dan password sesuai dalam users.txt
        sscanf(line, "%[^;];%s", storedUsername, storedPassword);
        if (strcmp(username, storedUsername) == 0 && strcmp(password, storedPassword) == 0) {
            isAuthenticated = 1;
            break;
        }
    }

    fclose(file);
    return isAuthenticated;
}

//hal yang didapatkan user saat sudah login
void akses(){

    char jawaban[20];
    while (1) {
        printf("Hello, What do you want to do? (Input: 'read' or 'rename' or 'exit'): ");
        scanf("%s", jawaban);

        //dapat langsung memunculkan isi dari folder rahasia yang disimpan dalam /usr/before
        if (strcmp(jawaban, "read") == 0) {
            system("docker exec -it container_rahasia /bin/bash -c 'ls /usr/before'");
        } 

        //memunculkan isi dari folder rahasia yang telah direname dan tersimpan dalam /usr/share
        else if (strcmp(jawaban, "rename") == 0) {
            system("docker exec -it container_rahasia /bin/bash -c 'ls /usr/share'");  
        } 

        else if(strcmp(jawaban, "exit") == 0){
            break;
        }
        
        else {
            printf("Invalid choice. Please try again.\n");
        }
    }
}


//login
void login(){
    
    puts("Welcome to Login Menu");

    char username[100];
    char password[100];
    char hashed_password[500];

    int isAuthenticated = 0;

    do {
        printf("Enter your username: ");
        scanf("%s", username);
        username[strcspn(username, "\n")] = '\0';
        
        printf("Enter your new password: ");
        //agar tidak muncul diterminal
        char* input = getpass("");
        strncpy(password, input, 100);

        //hapus input yang tersimpan di memori
        memset(input, 0, strlen(input));

        //menyimpan hasil md5 ke hashed_password
        strcpy(hashed_password, GetMD5String(password, sizeof(password)));

        if (checkCredentials(username, hashed_password)) {
            isAuthenticated = 1;
            printf("Login successful!\n");

            akses();
        } 
        else {
            printf("Invalid username or password. Please try again.\n");
        }
    } 
    while (!isAuthenticated);


};

//menyimpan user baru
void registerUser(const char *username, const char *password) {
    FILE *file = fopen("users.txt", "a");
    if (file == NULL) {
        printf("Failed to open users.txt\n");
        exit(1);
    }

    fprintf(file, "%s;%s\n", username, password);
    fclose(file);
}

//cek apakah user available
int isUsernameAvail(const char *username) {
    FILE *file = fopen("users.txt", "r");
    if (file == NULL) {
        printf("Failed to open users.txt\n");
        exit(1);
    }

    char line[100];
    while (fgets(line, sizeof(line), file)) {
        //cek apakah sama dengan username
        char *found = strstr(line, username);
        if (found != NULL) {
            fclose(file);
            return 1;
        }
    }

    fclose(file);

    return 0;
}

//regis
void regis(){

    puts("Welcome to Register Menu");

    char username[100];
    printf("Enter your new username: ");
    scanf("%s", username);
    username[strcspn(username, "\n")] = '\0';
    
    //cek apakah user yang dimasukkan berbeda
    while (isUsernameAvail(username)) {
        
        printf("Username is already taken. Please enter a different username: ");
        scanf("%s", username);
        username[strcspn(username, "\n")] = '\0';
    }
    
    char password[100];
    printf("Enter your new password: ");
    char* input = getpass("");
    strncpy(password, input, 100);

    //hapus input yang tersimpan di memori
    memset(input, 0, strlen(input));

    //membuat jadi md5 hashing
    char hashed_password[500];
    strcpy(hashed_password, GetMD5String(password, sizeof(password)));

    //menyimpan ke users.txt
    registerUser(username, hashed_password);

    printf("Registration successful. Username and password have been saved.\n");

    char choice[20];
    printf("Choose an option 'login' or 'done' : ");
    scanf("%s", choice);

    if (strcmp(choice, "login") == 0) {
        login();
    }

};

int countfolder = 0;
int countjpg = 0;
int countpdf = 0;
int counttxt= 0;
int countmp3 = 0;
int countpng = 0;
int countdocx = 0;
int countgif = 0;

//untuk rekursi menghitung banyaknya folder dan file
void countEntities(const char *pathh) {
    DIR *dir = opendir(pathh);
    if (dir == NULL) {
        printf("Failed to open directory: %s\n", pathh);
        return;
    }

    struct dirent *entry;
    //mengambil file dalam pathh
    while ((entry = readdir(dir)) != NULL) {
        // Skip "." and ".." entries
        if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0) {
            continue;
        }

        // Construct the full path of the entry
        char entryPath[1000];
        snprintf(entryPath, sizeof(entryPath), "%s/%s", pathh, entry->d_name);

        struct stat st;
        if (stat(entryPath, &st) == -1) {
            printf("Failed to get file information for: %s\n", entryPath);
            continue;
        }

        //cek apakah merupakan file
        if (S_ISREG(st.st_mode)) {
            if (strstr(entry->d_name, "pdf") != NULL) {
                countpdf++;
            } 
            
            else if (strstr(entry->d_name, "jpg") != NULL) {
                countjpg++;
            } 
            
            else if (strstr(entry->d_name, "txt") != NULL) {
                counttxt++;
            } 
            
            else if (strstr(entry->d_name, "mp3") != NULL) {
                countmp3++;
            } 
            
            else if (strstr(entry->d_name, "png") != NULL) {
                countpng++;
            } 
            
            else if (strstr(entry->d_name, "docx") != NULL) {
                countdocx++;
            } 
            
            else if (strstr(entry->d_name, "gif") != NULL) {
                countgif++;
            } 
            
            else {
                // Extension tidak sesuai dengan yang diharapkan, tambahkan penanganan kesalahan di sini
                printf("Extension tidak dikenali: %s\n", entry->d_name);
            }
        }

        //cek apakah merupakan directory
        else if (S_ISDIR(st.st_mode)) {
            countfolder++;

            // akan terjadi rekursi karena membaca isi dari subfolder
            countEntities(entryPath);
        }
    }

    closedir(dir);
}

int main(int  argc, char *argv[]){

    const char *url = "https://drive.google.com/uc?export=download&id=18YCFdG658SALaboVJUHQIqeamcfNY39a&confirm=t&uuid=7d5965a5-2291-4908-ba42-a15dbef98b89&at=AKKF8vzGZMUfNSeaB1slvn1Hy6ou:1684836666581";
    const char *outputPath = "/home/dhe/Documents/Diajeng/Programming/sisop/Praktikum4/rahasia.zip";

    //melakukan fork karena terjadi 2 proses yaitu pertama kita download file dulu lalu baru lanjut unzip dst
    pid_t pid = fork();

    if(pid == 0){ //child process

        download_file(url, outputPath);
    }
    else{

        wait(NULL);

        //karena folder rahasia gabisa kesimpen kalau ada compiler bernama rahasia
        remove("rahasia");

        //melakukan unzip
        unzip_file(outputPath);

        //melakukan copy folder karena untuk rename
        copyFolder();

        //menghitung jumlah file dan folder
        const char *pathh = "/home/dhe/Documents/Diajeng/Programming/sisop/Praktikum4/rahasia";
        countEntities(pathh);

        //menyimpan tree
        system("sudo touch extension.txt");
        system("sudo chmod 777 extension.txt");

        FILE *file = fopen("extension.txt", "w");
        if (file == NULL) {
            printf("Failed to open file.\n");
            return 1;
        }

        fprintf(file, "pdf = %d\n", countpdf);
        fprintf(file, "jpg = %d\n", countjpg);
        fprintf(file, "folder = %d\n", countfolder);
        fprintf(file, "txt = %d\n", counttxt);
        fprintf(file, "mp3 = %d\n", countmp3);
        fprintf(file, "png = %d\n", countpng);
        fprintf(file, "docx = %d\n", countdocx);
        fprintf(file, "gif = %d\n", countgif);

        fclose(file);

        //terjadi fork lagi agar hasil dari fuse (berupa rename) dapat tersimpan di docker container
        pid_t pid2 = fork();

        if(pid2 == 0){
            //hak akses default tidak akan berubah
            umask(0);

            //alamat dari struktur xmp_oper yang berisi implementasi dari operasi-operasi filesystem
            //NULL berarti tidak ada data tambahan yang diteruskan ke filesystem
            fuse_main(argc, argv, &xmp_oper, NULL);
        }

        else{
            //kita pull imagenya dulu lalu build docker-compose
            system("docker pull ubuntu:focal");
            system("docker-compose up -d");

            system("sudo touch users.txt");
            system("sudo chmod 777 users.txt");

            printf("Welcome to main menu!!\n");
            printf("Please choose between login or register (input: 'login' or 'register'): ");

            char temp[20];
            scanf("%s", temp);

            if (strcmp(temp, "register") == 0) {
                regis();
            } 
            
            else if(strcmp(temp, "login") == 0) {
                login();
            }

            else{

                while (strcmp(temp, "login") != 0 && strcmp(temp, "register") != 0) {

                    printf("Invalid choice! Please enter either 'login' or 'register': ");
                    scanf("%s", temp);
                }

                if (strcmp(temp, "login") == 0) {

                    login();
                } 
                
                else if (strcmp(temp, "register") == 0) {
                    regis();
                }
            }
        }

    }

    return 0;

}

//gcc -Wall `pkg-config fuse --cflags` rahasia.c -o rahasia `pkg-config fuse --libs` -lm
//https://www.programmingalgorithms.com/algorithm/md5/
